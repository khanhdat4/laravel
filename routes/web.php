<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::get('/', function () {
    return view('/login');
});
Route::resource('/tasks', TaskController::class);
// Route::prefix('/tasks')->name('tasks.')->group(function () {
//     Route::get('/', [TaskController::class, 'index'])->name('index');
//     Route::post('/', [TaskController::class, 'store'])->name('store');
//     Route::get('/create', [TaskController::class, 'create'])->name('create');
//     Route::get('/{id}', [TaskController::class, 'show'])->name('show');
//     Route::put('/{id}', [TaskController::class, 'update'])->name('update');
//     Route::delete('/{id}', [TaskController::class, 'destroy'])->name('destroy');
//     Route::put('/{id}/edit', [TaskController::class, 'edit'])->name('edit');
// });
Route::get('/login', [LoginController::class, 'show'])->name('login');
Route::post('/login', [LoginController::class, 'authenticate'])->name('login.authenticate');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::fallback(function(){ abort(404); });

