<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function show()
    {
        if (auth()->check()) {
            return redirect()->route('tasks.index');
        } else {
            return view('login');
        }
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (auth()->attempt($credentials)) {
            return redirect()->route('tasks.index');
        }
        return redirect()->back()->with('errors', 'email or password wrong');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login');
    }
}
