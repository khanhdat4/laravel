<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $existsSpecialCharacterPerCent = preg_match('/[%]/', $search);
        $search = preg_replace('/[%]/', '\%', $search);
        $search = '%'.preg_replace('/\s+/', ' ', $search).'%';
        $tasks = Task::where('name', 'like', $search)
                ->orWhere('content', 'like', $search)
                ->latest('id')
                ->paginate(5)->withQueryString();

        return response()->view('tasks.index', compact('tasks'), 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return response()->view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TaskRequest $request)
    {
        $dataCreate = $request->all();

        $this->task->create($dataCreate);

        return redirect()->route('tasks.index')->withStatus(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $task = Task::findOrFail($id);
        return response()->view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        $task = Task::findOrFail($id);

        $queryBeforeUpdate = explode('?', url()->previous())[1];
        if (isset($queryBeforeUpdate)) {
            session(['queryURL' => $queryBeforeUpdate]);
        }
        return response()->view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TaskRequest $request, string $id)
    {
        $task = Task::findOrFail($id);
        $dataUpdate = [
            'name' => $request->name,
            'content' => $request->content,
        ];

        $queryURL = explode('&', session('queryURL'));
        $task->update($dataUpdate);
        return redirect()->route('tasks.index', $queryURL)->withStatus(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $task = Task::findOrFail($id);

        $task->delete();
        session()->flash('message', 'delete success');

        return back();
    }
}
