<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     */

    public function index()
    {
        $post = Post::all();
        $postCollection = PostResource::collection($post);

        return $this->setSuccessResponse($postCollection);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostRequest $request)
    {
        $dataCreate = $request->all();
        $post = $this->post->create($dataCreate);
        $postResource = new PostResource($post);

        return $this->setSuccessResponse($postResource);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $post = $this->post->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'errors' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }
        $postResource = new PostResource($post);

        return $this->setSuccessResponse($postResource);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostRequest $request, string $id)
    {
        try {
            $post = $this->post->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'errors' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }
        $dataCreate = $request->all();
        $post->update($dataCreate);
        $postResource = new PostResource($post);

        return $this->setSuccessResponse($postResource);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $post = $this->post->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'errors' => $e->getMessage()
            ], Response::HTTP_NOT_FOUND);
        }

        $post->delete();

        $postResource = new PostResource($post);

        return $this->setSuccessResponse($postResource);
    }
}
