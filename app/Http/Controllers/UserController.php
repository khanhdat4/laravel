<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Response;

class UserController extends Controller
{

    public function index()
    {
        $userCollection = UserResource::collection(User::all());
        return response()->json([
            'data' => $userCollection,
        ], Response::HTTP_OK);
    }

    public function show($id)
    {
        $user = User::find($id);
        if (!empty($user)) {
            return response()->json([
                'data' => new UserResource($user),
            ], Response::HTTP_OK);
        }

        return response()->json([
            'message' => 'Not found',
        ], Response::HTTP_NOT_FOUND);
    }
}
