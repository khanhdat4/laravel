@extends('master')
@section('main')
    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <table class="table table-hover">
        <tr class="text-center">
            <th>ID</th>
            <th>Name</th>
            <th>Content</th>
            <th>Action</th>
        </tr>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ $task->id }}</td>
                <td><a href="{{ route('tasks.show', $task->id) }}">{{ $task->name }}</a></td>
                <td>{{ $task->content }}</td>
                <td>
                    <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-primary">Update</a>
                    <form action="{{ route('tasks.destroy', $task->id) }}" method="POST" onsubmit="return confirm('are you sure?')">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    {{$tasks->links()}}
    </div>
@endsection
