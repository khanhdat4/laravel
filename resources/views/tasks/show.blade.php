@extends('master')
@section('main')
    <table class="table table-hover">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Content</th>
            <th>Action</th>
        </tr>
        <tr>
            <td>{{ $task->id }}</td>
                <td><a href="{{ route('tasks.show', $task->id) }}">{{ $task->name }}</a></td>
                <td>{{ $task->content }}</td>
                <td>
                    <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-primary">Update</a>
                    <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-warning">Delete</button>
                    </form>
                </td>
        </tr>
    </table>
@endsection
