@extends('master')
@section('main')
    <div class="container">
        <h2 class="text-center">Login</h2>
        <div class="container d-flex justify-content-center">
            <form action="{{ route('login.authenticate') }}" style="max-width: 25rem;" method="POST">
                @csrf
                @if (session('errors'))
                    <div class="alert alert-danger">{{ session('errors') }}</div>
                @endif
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
