<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTaskTest extends TestCase
{
    /** @test */
    public function user_search_by_name()
    {
        // $task = Task::factory()->create();

        $response = $this->get(route('tasks.index'), ['McLaughlin']);

        $response->assertStatus(200);
        $response->assertViewIs('tasks.index');
        // $response->assertSee([]);
    }

    /** @test */
    public function user_search_by_content()
    {
        // $task = Task::factory()->create();
        // $search = str_split($task->content, 5)[0];
        $response = $this->get(route('tasks.index'), ['dat123321']);

        $response->assertStatus(200);
        $response->assertViewIs('tasks.index');
        // $response->assertSee([$task->name, $task->content]);
    }


}
