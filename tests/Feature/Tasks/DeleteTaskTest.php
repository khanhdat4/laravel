<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_delete_task_if_task_exist()
    {
        $this->actingAs(User::factory()->create());
        // $task = Task::factory()->create();

        $response = $this->delete(route('tasks.destroy', 32));
        $response->assertSessionHas('message');
    }

    /** @test */
    public function authenticate_user_can_not_delete_task_if_task_not_exist()
    {
        $id = -1;
        $this->actingAs(User::factory()->create());

        $response = $this->delete(route('tasks.destroy', 33));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function unauthenticate_user_can_not_delete_task()
    {

        $response = $this->delete(route('tasks.destroy', 30));
        $response->assertRedirect('/login');
    }
}
