<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    public function getRouteStore()
    {
        return route('tasks.store');
    }

    /** @test */
    public function authenticate_user_can_create_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getRouteStore(), $task);

        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $task);
    }

    /** @test */
    public function unauthenticate_user_can_not_create_task()
    {
        $task = Task::factory()->make()->toArray();
        $response = $this->post($this->getRouteStore(), $task);

        $response->assertRedirect('/login');
    }

    /** @test */
    public function unauthenticate_user_can_not_see_create_task_form()
    {
        $response = $this->get(route('tasks.create'));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null])->toArray();
        $response = $this->post($this->getRouteStore(), $task);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_content_field_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['content' => null])->toArray();
        $response = $this->post($this->getRouteStore(), $task);
        $response->assertSessionHasErrors(['content']);
    }

    /** @test */
    public function authenticate_user_can_not_create_task_if_data_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = ['name' => null, 'content' => null];
        $response = $this->post($this->getRouteStore(), $task);
        $response->assertSessionHasErrors(['content', 'name']);
    }
}
