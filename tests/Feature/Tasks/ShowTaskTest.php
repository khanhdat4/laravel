<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    /** @test */
    public function user_can_show_task_detail()
    {
        // $task = Task::factory()->create();

        $response = $this->get(route('tasks.show', 24));

        $response->assertOk();
        $response->assertViewIs('tasks.show');
        // $response->assertSee([$task->name, $task->content]);
    }
}
