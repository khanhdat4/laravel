<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function user_can_get_list_task()
    {
        $response = $this->get(route('tasks.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
    }
}
