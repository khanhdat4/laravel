<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function authenticate_user_can_update_task_if_data_valid()
    {
        $this->actingAs(User::factory()->create());

        // $task = Task::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'content' => fake()->text(),
        ];

        $response = $this->put(route('tasks.update', 23), $dataUpdate);
        // dd(route('tasks.update', 23));
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $dataUpdate);
    }

    /** @test */
    public function unauthenticate_user_can_not_update_task()
    {
        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'content' => fake()->text(),
        ];

        $response = $this->put(route('tasks.update', $task->id), $dataUpdate);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_name_field_is_null()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => null,
            'content' => fake()->text(),
        ];

        $response = $this->put(route('tasks.update', $task->id), $dataUpdate);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_content_field_is_null()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'content' => null,
        ];

        $response = $this->put(route('tasks.update', $task->id), $dataUpdate);
        $response->assertSessionHasErrors(['content']);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_data_is_null()
    {
        $this->actingAs(User::factory()->create());

        $task = Task::factory()->create();
        $dataUpdate = [
            'name' => null,
            'content' => null,
        ];

        $response = $this->put(route('tasks.update', $task->id), $dataUpdate);
        $response->assertSessionHasErrors(['name', 'content']);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_id_not_exist()
    {
        $id = -1;
        $this->actingAs(User::factory()->create());
        $dataUpdate = [
            'name' => fake()->name(),
            'content' => fake()->text(),
        ];

        $response = $this->json('PUT', route('tasks.update', $id), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
