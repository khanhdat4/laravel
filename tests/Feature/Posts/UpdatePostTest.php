<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    /** @test */
    public function user_can_update_if_data_is_valid()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'body' => fake()->text(),
        ];

        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                    ->where('body', $dataUpdate['body'])
                    ->etc()
            )
        );

        $this->assertDatabaseHas('posts', [
            'name' => $dataUpdate['name'],
            'body' => $dataUpdate['body'],
        ]);
    }

    /** @test */
    public function user_can_not_update_if_id_not_found()
    {
        $id = -1;
        $dataUpdate = [
            'name' => fake()->name(),
            'body' => fake()->text(),
        ];

        $response = $this->json('PUT', route('posts.update', $id), $dataUpdate);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors')
        );
    }

    /** @test */
    public function user_can_not_update_if_name_and_body_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => '',
        ];

        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name')
                    ->has('body')
            )
        );
    }

    /** @test */
    public function user_can_not_update_if_name_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => '',
            'body' => fake()->text(),
        ];

        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('name')
            )
        );
    }

    /** @test */
    public function user_can_not_update_if_body_is_null()
    {
        $post = Post::factory()->create();
        $dataUpdate = [
            'name' => fake()->name(),
            'body' => '',
        ];

        $response = $this->json('PUT', route('posts.update', $post->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors', fn(AssertableJson $json) =>
                $json->has('body')
            )
        );
    }
}
