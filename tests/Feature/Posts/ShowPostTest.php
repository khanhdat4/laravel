<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class ShowPostTest extends TestCase
{
    /** @test */
    public function user_can_get_post_if_id_exist()
    {
        $post = Post::factory()->create();

        $response = $this->json('GET', route('posts.show', $post->id));

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->has('name')
                    ->has('body')
                    ->etc()
            )
        );
    }

    /** @test */
    public function user_can_not_get_post_if_id_not_exist()
    {
        $id = -1;

        $response = $this->json('GET', route('posts.show', $id));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors'));
    }
}
