<?php

namespace Tests\Feature\Posts;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_post()
    {
        $response = $this->getJson(route('posts.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data')
        );
    }
}
