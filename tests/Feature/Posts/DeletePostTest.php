<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_if_id_exists()
    {
        $post = Post::factory()->create();

        $response = $this->json('DELETE', route('posts.destroy', $post->id));
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('id', $post->id)
                    ->where('name', $post->name)
                    ->where('body', $post->body)
            )
        );
    }

    /** @test */
    public function user_can_not_delete_if_id_not_exists()
    {
        $id = -1;

        $response = $this->json('DELETE', route('posts.destroy', $id));
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('errors'));
    }
}
